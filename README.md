Derivated themes to follow the Fantome GTK theme from [Addi's gtk-theme-collections](https://github.com/addy-dclxvi/gtk-theme-collections).

![screenshot](screenshot.png)

Openbox-3 theme is derived from the Arc theme.

`xresources` file is to be included in xrdb environment file. It sets main colors (foreground and background) and rxvt-unicode cursor and scrollbar colors.
